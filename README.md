# Lenovo Z5s sources

![Android](https://img.shields.io/badge/Android-3DDC84?style=for-the-badge&logo=android&logoColor=white) ![AOSP](https://img.shields.io/badge/AOSP-3DDC84?style=for-the-badge&logo=android&logoColor=white) 
[![Telegram Updates](https://img.shields.io/badge/Updates-2CA5E0?style=for-the-badge&logo=telegram&logoColor=white)](https://t.me/z5supdates) [![Telegram Chat](https://img.shields.io/badge/Chat-2CA5E0?style=for-the-badge&logo=telegram&logoColor=white)](https://t.me/lenovoz5s)

## Fast cloning

```git clone https://gitlab.com/lenovo-z5s/device_lenovo_sdm710-common device/lenovo/sdm710-common && git clone https://gitlab.com/lenovo-z5s/device_lenovo_jd2019 device/lenovo/jd2019 && git clone https://gitlab.com/lenovo-z5s/vendor_lenovo_jd2019 vendor/lenovo/jd2019 && git clone https://gitlab.com/lenovo-z5s/vendor_lenovo_sdm710-common vendor/lenovo/sdm710-common && git clone https://gitlab.com/victor10520/vendor_lenovo-firmware.git vendor/lenovo-firmware && git clone https://github.com/lenovo-sdm710/kernel_lenovo_sdm710 kernel/lenovo/sdm710```
